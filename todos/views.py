from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem


# Create your views here.


# def show_todolist(request,id):
#     todolist = get_object_or_404(TodoList, id=id)
#     context = {
#         "todolist_object": todolist,
#     }
#     return render(request, )

def todolist(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render (request, "list.html", context)
